package main.java.com.escale.dojo;

public class Player extends Employee {

  int number;

  public Player(String name, double salary, int number) {
    super(name, salary);
    this.number = number;
  }

  @Override
  public void whoami() {
    System.out.println(String.format("My name is %s and I\'m number %d", name, number));
  }
}
