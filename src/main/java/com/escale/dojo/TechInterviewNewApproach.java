package main.java.com.escale.dojo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TechInterviewNewApproach {

  private int limit;

  public Map<Integer, Integer> solve(final Integer threshold, final Integer... input) {
    this.limit = threshold;

    final long st = System.nanoTime();
    final List<Integer> newInput = Arrays.asList(input);

    try {
      HashSet<Integer> founded = findValue(newInput);
      return findResult(newInput, founded);
    } finally {
      System.out.println(
          String.format("Execution time: %.3fms", (System.nanoTime() - st) / 1000F / 1000F));
    }
  }

  private HashSet<Integer> findValue(List<Integer> values) {
    return values.stream()
        .filter(i -> i > limit)
        .filter(this::isHalfOfThreshold)
        .collect(Collectors.toCollection(HashSet::new));
  }

  private Map<Integer, Integer> findResult(List<Integer> values, HashSet<Integer> founded) {
    Map<Integer, Integer> result = new LinkedHashMap<>();

    values.forEach(i -> {
      int compare = createComparative(limit, i);
      if (values.contains(compare) && !founded.contains(i)) {
        founded.add(i);
        founded.add(compare);
        result.put(i, compare);
      }
    });

    return result;
  }

  private boolean isHalfOfThreshold(int value) {
    return this.limit / 2F == value;
  }

  private int createComparative(int threshold, int value) {
    return threshold - value;
  }
}
