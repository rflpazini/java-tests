package main.java.com.escale.dojo;

public abstract class Employee {

  String name;
  double salary;

  public Employee(String name, double salary) {
    this.name = name;
    this.salary = salary;
  }

  public abstract void whoami();
}
