package main.java.com.escale.dojo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TechInterview {

  public Map<Integer, Integer> solve(final Integer threshold, final Integer... input) {
    final long st = System.nanoTime();
    final List<Integer> newInput = Arrays.asList(input);

    try {
      final Set<Integer> found = new HashSet<>();
      final Map<Integer, Integer> result = new LinkedHashMap<>();

      for (Integer item : newInput) {
        if (item > threshold) {
          continue;
        }

        final Boolean isHalf = threshold / 2F == item;

        if (isHalf) {
          if (!found.contains(item)) {
            found.add(item);
          } else {
            result.put(item, item);
          }
        } else {
          final Integer compare = threshold - item;

          if (newInput.contains(compare) && !found.contains(compare)) {
            found.add(item);
            found.add(compare);

            result.put(item, compare);
          }
        }
      }

      return result;
    } finally {
      System.out.println(
          String.format("Execution time: %.3fms", (System.nanoTime() - st) / 1000F / 1000F));
    }
  }
}
